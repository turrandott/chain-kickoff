#!/bin/bash

# Check if Docker is already installed
if ! [ -x "$(command -v docker)" ]; then
  echo "Docker is not installed. Installing Docker..."
  sudo apt update
  sudo apt install -y docker.io
fi

# # Check if docker-compose-plugin is already installed
# if ! [ -x "$(command -v docker-compose)" ]; then
#   echo "docker-compose is not installed. Installing docker-compose..."
#   sudo apt update
#   sudo apt install -y docker-compose
# fi

# Check if Node.js is already installed
if ! [ -x "$(command -v node)" ]; then
  echo "Node.js is not installed. Installing Node.js..."
  sudo apt update
  sudo apt install -y nodejs
fi

# Download files from GitLab
echo "Downloading files..."
# curl -LJO https://gitlab.com/turrandott/chain-kickoff/-/raw/main/files/docker-compose.yml -o docker-compose.yml
mkdir node
cd node
curl -LJO https://gitlab.com/turrandott/chain-kickoff/-/raw/main/files/node.cfg -o node.cfg
curl -LJO https://gitlab.com/turrandott/chain-kickoff/-/raw/main/files/xriba.json -o xriba.json
mkdir data
cd data
curl -LJO https://gitlab.com/turrandott/chain-kickoff/-/raw/main/files/keystore.js -o keystore.js

# Generate keystore using Node.js
echo "Generating keystore using Node.js..."
echo "=============================================================="
echo "============IMPORTANT: save following info===================="
echo "=============================================================="
node keystore.js
echo "=============================================================="
echo "IMPORTANT: don't forget to save mnemonic phrase and private key"
echo "==============================================================="
rm keystore.js
cd ../..

# Get address from UTC file and replace in node.cfg
echo "Getting address and password file names from keystore and updating node.cfg..."
UTC_FILE=$(ls node/data/keystore | grep "UTC-.*")
ADDRESS=$(grep -oP '"address":"\K[^"]+' node/data/keystore/$UTC_FILE)
ADDRESS="0x$ADDRESS"

# Replace all "YOUR_ADDRESS" in node.cfg with the generated address
sed -i "s/YOUR_ADDRESS/$ADDRESS/g" node/node.cfg

# Get password file name from keystore and replace in node.cfg
PASSWORD_FILE=$(ls node/data/keystore | grep "password-.*")
sed -i "s/YOUR_PASSWORD_FILE/$PASSWORD_FILE/g" node/node.cfg

# Download Docker image
echo "Downloading Docker image..."
sudo docker pull xriba/nethermind:latest

# # Launch Docker Node
# echo "Launching the node..."
PATH_TO_FOLDER=$(pwd)

echo sudo docker run -it --name node -p 8545:8545 -p 30300:30300 -p 30300:30300/udp --network host -v $PATH_TO_FOLDER/node/xriba.json:/nethermind/chainspec/xriba.json -v $PATH_TO_FOLDER/node/node.cfg:/nethermind/configs/node.cfg -v $PATH_TO_FOLDER/node/data:/nethermind/data xriba/nethermind --datadir data --config node

sudo docker run -it --name node -p 8545:8545 -p 30300:30300 -p 30300:30300/udp --network host -v $PATH_TO_FOLDER/node/xriba.json:/nethermind/chainspec/xriba.json -v $PATH_TO_FOLDER/node/node.cfg:/nethermind/configs/node.cfg -v $PATH_TO_FOLDER/node/data:/nethermind/data xriba/nethermind --datadir data --config node
