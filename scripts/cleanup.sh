#!/bin/bash

# Stop and remove Docker container
sudo docker-compose down
sudo docker rmi $(sudo docker images -q)

# Remove Node.js and Docker
sudo apt-get remove -y docker.io nodejs
sudo apt-get autoremove -y
sudo apt-get autoclean -y
sudo apt-get clean -y

# Remove files
rm -rf node
