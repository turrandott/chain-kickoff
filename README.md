# chain-kickoff
### Prerequesities
ubuntu 22.10

## Getting started
Download kickoff.sh
```curl -LJO https://gitlab.com/turrandott/chain-kickoff/-/raw/main/scripts/kickoff.sh```
Make sure to give the script executable permissions (chmod +x kickoff.sh) before running it.

## Prepare your node
```curl https://gitlab.com/turrandott/chain-kickoff/-/raw/main/scripts/cleanup.sh```
run ```./kickoff.sh```

## Clean up
To uninstall and delete files download cleanup.sh
run ```./cleanup.sh```

### only some files:
rm -rf node/data/keystore
sudo docker rm node

ssh ubuntu@145.239.198.194